import asyncio
import logging
from pathlib import Path

import click

from .database import add_sample
from .watcher import watch

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


async def samplefinder(path: Path):
    if path.is_dir():
        await add_sample(path.name, path.parent.stem)
    return path


async def _main(*path):
    tasks = [asyncio.create_task(watch(p, "", "INGESTED", samplefinder)) for p in path]
    await asyncio.wait(tasks)


@click.command()
@click.argument("dir", nargs=-1)
def main(dir):
    asyncio.run(_main(*dir))
