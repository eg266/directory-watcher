import os

import click
from kombu import Exchange, Queue

from celery import Celery

exchange_name = "zarr"
exchange_type = "direct"
broker = os.getenv("BROKER", "amqp://")

exchange = Exchange(exchange_name, type=exchange_type)
queue_zarr = Queue("zarr", exchange, routing_key="zarr")

app = Celery(
    "raw2zarr",
    broker=f"{broker}",
    backend="rpc://",
    include=["watcher.tasks"],
)


app.conf.task_queues = (queue_zarr,)
app.conf.task_default_queue = "zarr"
app.conf.task_default_exchange = "zarr"
app.conf.task_default_routing_key = "zarr"
app.conf.worker_prefetch_multiplier = 1
app.conf.task_acks_late = True
app.conf.worker_send_task_events = True
app.conf.update(
    result_expires=3600,
)


@click.command()
@click.option("--queues", help="queue to listen to")
@click.option("--concurrency", help="concurrency")
def main(queues, concurrency):
    cmd = ["worker", "-l", "info"]
    if queues:
        cmd += ["--queues", queues]
    if concurrency:
        cmd += ["--concurrency", concurrency]
    app.worker_main(cmd)
