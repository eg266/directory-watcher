import asyncio
from contextlib import suppress
from datetime import datetime

import asyncpg

from .settings import Settings

"""
CREATE TABLE IF NOT EXISTS uploader (
    id SERIAL PRIMARY KEY,
    user_id INTEGER,
    name VARCHAR(255) NOT NULL,
    instrument VARCHAR(80) NOT NULL,
    ok_to_copy BOOLEAN,
    valid BOOLEAN,
    zarr_converted BOOLEAN,
    owl_submitted BOOLEAN,
    date_ok timestamp,
    date_valid timestamp,
    date_zarr timestamp,
    date_owl timestamp,
    error TEXT,
    UNIQUE (name, instrument)
);

CREATE OR REPLACE FUNCTION notify_uploader() RETURNS TRIGGER AS $$
    BEGIN
    PERFORM pg_notify('uploader',row_to_json(NEW)::TEXT);
    RETURN NULL;
    END;
$$ LANGUAGE plpgsql;
;

CREATE TRIGGER trigger_uploader
  AFTER UPDATE
  ON uploader
  FOR EACH ROW
  EXECUTE PROCEDURE notify_uploader();

"""

# TODO: name is not unique, so this may fail. Add instrument to all calls.


async def add_sample(name, instrument):
    conn = await asyncpg.connect(Settings.db_url)
    with suppress(Exception):  # TODO: user proper exception here
        await conn.execute(
            "INSERT INTO uploader(name, instrument) VALUES($1, $2)", name, instrument
        )
    await conn.close()


async def update_error(name, error):
    conn = await asyncpg.connect(Settings.db_url)
    await conn.execute("UPDATE uploader SET error=$1 WHERE name=$2", error, name)
    await conn.close()


async def update_valid(name, is_valid):
    conn = await asyncpg.connect(Settings.db_url)
    await conn.execute(
        "UPDATE uploader SET valid=$1, date_valid=$2 WHERE name=$3",
        is_valid,
        datetime.now(),
        name,
    )
    await conn.close()


async def update_zarr(name, is_zarr):
    conn = await asyncpg.connect(Settings.db_url)
    await conn.execute(
        "UPDATE uploader SET zarr_converted=$1, date_zarr=$2 WHERE name=$3",
        is_zarr,
        datetime.now(),
        name,
    )
    await conn.close()


async def update_pipeline(name, is_owl):
    conn = await asyncpg.connect(Settings.db_url)
    await conn.execute(
        "UPDATE uploader SET owl_submitted=$1, date_owl=$2 WHERE name=$3",
        is_owl,
        datetime.now(),
        name,
    )
    await conn.close()
