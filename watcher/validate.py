import asyncio
import logging

import click

from .watcher import watch

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from .database import update_error, update_valid


async def validate(path):
    """Run validation tasks on the datasets."""
    logger.info("Validating %s", path)
    # TODO: For the moment everything validates
    if True:
        await update_valid(path.name, True)
        await update_error(path.name, None)
    else:
        await update_valid(path.name, False)
        raise Exception("Invalid data")
    return path


async def _main(*path):
    tasks = [
        asyncio.create_task(watch(p, "OK_TO_COPY", "VALID", validate)) for p in path
    ]
    await asyncio.wait(tasks)


@click.command()
@click.argument("dir", nargs=-1)
def main(dir):
    asyncio.run(_main(*dir))
