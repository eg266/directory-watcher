from contextlib import suppress


class UploaderException(Exception):
    def __init__(self, path, message):
        self.path = path
        self.message = message
        super().__init__(self.message)

        with suppress(Exception):
            with open(self.path / "ERROR", "w") as fh:
                fh.write(self.message)
