import asyncio
import json
import os
from pathlib import Path

import asyncpg
import click

from .settings import Settings

DATA_DIR = os.getenv("DATA_DIR", "/data/incoming")


def foo(conn, pid, channel, payload):
    payload = json.loads(payload)
    print(channel, payload)
    if payload["ok_to_copy"]:
        name, instrument = payload["name"], payload["instrument"]
        p = Path(DATA_DIR) / instrument / name
        if p.exists() and not (p / "OK_TO_COPY").exists():
            (p / "OK_TO_COPY").touch()


async def listen_for_changes(callback):
    conn = await asyncpg.connect(Settings.db_url)
    await conn.add_listener("uploader", callback)
    while True:
        await asyncio.sleep(10)
        await conn.execute("SELECT 1")


@click.command()
def main():
    asyncio.run(listen_for_changes(foo))
