import asyncio
import logging
import traceback
from pathlib import Path
from typing import Callable

from .database import add_sample, update_error
from .errors import UploaderException
from .slackbot import postMessage

# from .database import create_database, update_database
# from .settings import Settings
# from .slackbot import postMessage


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

POLL_SECONDS = 10


async def process(path, func: Callable, msg=None):
    """Watch directory for OK_TO_COPY and process"""
    logger.info("Processing %s", path)

    if not path.exists():
        await asyncio.sleep(0)
        raise FileNotFoundError(path)

    await asyncio.sleep(POLL_SECONDS)

    try:
        await func(path)
        if msg:
            await postMessage(msg)
    except Exception as e:
        err = traceback.format_exc()
        logger.critical("Invalid data in %s: %s", path, err)
        msg = f"PROCESS[{func.__name__}]: {path}: {err}"
        await update_error(path.name, msg)
        raise UploaderException(path, msg) from e

    return path


async def watch(
    path,
    flag_start: str,
    flag_end: str,
    func: Callable,
    msg=None,
    ntasks=100,
):
    """Check for subdirectories created inside main directory"""
    logger.info("Watching %s", path)
    # create_database()
    p = Path(path)
    names_proc = []
    tasks = []
    while True:
        await asyncio.sleep(POLL_SECONDS)
        if Path("/tmp/PAUSE").exists():
            logger.info("watcher paused")
            continue
        names_done = [
            item
            for item in Path(p).glob("*")
            if item.is_dir() and (item / flag_end).exists()
        ]

        # Check for new items
        names = names_done + names_proc

        for item in Path(p).glob("*"):
            if len(tasks) >= ntasks:
                continue
            if item.is_dir() and (item not in names):
                if not (item / "ERROR").exists():
                    if not (item / flag_end).exists():
                        if (flag_start is not None) and (item / flag_start).exists():
                            logger.info("New directory found %s", item)
                            names_proc.append(item)
                            tasks.append(asyncio.create_task(process(item, func, msg)))

        for task in tasks:
            if task.done():
                if task.exception():
                    e = task.exception()
                    if isinstance(e, UploaderException):
                        path = e.path
                        logger.error("Failed %s", path)
                else:
                    path = task.result()
                    (path / flag_end).touch()
                    logger.info("Completed %s", path)

                names_proc = [item for item in names_proc if item != path]

        tasks = [task for task in tasks if not task.done()]
