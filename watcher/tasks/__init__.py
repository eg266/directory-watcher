from pathlib import Path

from distributed import Client, LocalCluster

from ..celery import app
from ..errors import UploaderException


def _stpt2zarr(input_dir, output_dir):
    from stpt2zarr import stpt2zarr

    res = stpt2zarr(input_dir, output_dir)
    (Path(res) / ".zarr_done").touch()
    return res


def _imc2zarr(input_dir, output_dir):
    from imc2zarr import imc2zarr

    res = imc2zarr(input_dir, output_dir)
    (Path(res) / ".zarr_done").touch()
    return res


def _axio2zarr(input_dir, output_dir):
    from axio2zarr import axio2zarr

    res = axio2zarr(input_dir, output_dir)
    (Path(res) / ".zarr_done").touch()
    return res


@app.task
def convert2zarr(input_dir, output_dir):
    input_dir = Path(input_dir)
    output_dir = Path(output_dir)
    # TODO: maybe get instrument from the database before calling this function
    if "stpt" in f"{input_dir}":
        res = _stpt2zarr(input_dir, output_dir / "stpt")
    elif "imc" in f"{input_dir}":
        res = _imc2zarr(input_dir, output_dir / "imc")
    elif "axio" in f"{input_dir}":
        res = _axio2zarr(input_dir, output_dir / "axio")
    else:
        raise UploaderException(f"Unknown input_dir: {input_dir}")

    return res
