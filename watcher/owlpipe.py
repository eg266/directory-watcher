import asyncio
import logging
import os
from functools import partial

import aiohttp
import click
import yaml

from .database import update_pipeline
from .watcher import watch

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

OWL_USERNAME = "daemon"
OWL_PASSWORD = os.getenv("OWL_PASSWORD", "")
OWL_SECRET = os.getenv("OWL_SECRET", "")
OWL_API = os.getenv("OWL_URL", "imaxt.ast.cam.ac.uk/owl")


async def owl_submit(path, session):
    logger.info("Submitting to Owl %s", path)

    url = f"https://{OWL_API}/api/pipeline/add2"
    with (path.parent / "pipeline.yaml").open() as fh:
        conf = fh.read()
    conf = conf.replace("XXXX", path.name)
    conf = yaml.safe_load(conf)

    logger.info("Submitting to Owl %s", conf)

    async with session.post(
        url, json={"config": conf, "extra": {"username": OWL_USERNAME}}
    ) as response:
        await response.text()

    return path


async def _main(*path):
    async with aiohttp.ClientSession() as session:
        func = partial(owl_submit, session=session)
        tasks = [
            asyncio.create_task(
                watch(p, ".zarr_done", ".owl_done", func, "OWL_SUBMITTED")
            )
            for p in path
        ]
        await asyncio.wait(tasks)


@click.command()
@click.argument("dir", nargs=-1)
def main(dir):
    asyncio.run(_main(*dir))
