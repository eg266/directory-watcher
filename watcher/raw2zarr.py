import asyncio
import logging
import os

import click

from .database import update_error, update_zarr
from .tasks import convert2zarr
from .watcher import watch

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

DATA_DIR = os.getenv("DATA_RAW_DIR", "/data/raw")


async def raw2zarr(path):
    logger.info("Converting to ZARR %s", path)
    res = convert2zarr.s(f"{path}", f"{DATA_DIR}").apply_async()
    while not res.result:
        await asyncio.sleep(60)
    if res.status != "SUCCESS":
        await update_zarr(path.name, False)
        raise res.result
    await update_zarr(path.name, True)
    await update_error(path.name, None)
    return path


async def _main(*path):
    tasks = [
        asyncio.create_task(
            watch(p, "VALID", "ZARR_SUCCESSFUL", raw2zarr, "CONVERTED_ZARR")
        )
        for p in path
    ]
    await asyncio.wait(tasks)


@click.command()
@click.argument("dir", nargs=-1)
def main(dir):
    asyncio.run(_main(*dir))
