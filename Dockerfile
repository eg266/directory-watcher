ARG BASE_CONTAINER=continuumio/miniconda3:4.10.3
FROM $BASE_CONTAINER

LABEL maintainer="Eduardo Gonzalez-Solares <eglez@ast.cam.ac.uk>"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install -yq --no-install-recommends \
    sudo vim less && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN pip install -U pip && \
    pip install \
    numpy scipy pandas \
    lz4 blosc \
    msgpack-numpy msgpack-python \
    numcodecs \
    xlrd python-snappy dask distributed xarray \
    celery && \
    rm -rf /root/.cache/pip


ADD start.sh /usr/local/bin/start.sh
RUN chmod +x /usr/local/bin/start.sh


RUN addgroup --gid 1000 user && \
    adduser --home /home/user --uid 1000 --gid 1000 --disabled-password --gecos None user

WORKDIR /home/user
USER user

ENTRYPOINT [ "/usr/local/bin/start.sh" ]
